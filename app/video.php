<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class video extends Model
{
    /**
     * Table to use
     */
    protected $table = 'videos';
}

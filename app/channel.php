<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class channel extends Model
{
    /**
     * Table to use
     */
    protected $table = 'channels';
}

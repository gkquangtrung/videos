<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use DateTime;
use Session;
class General {
    /**
     * @param int $user_id User-id
     *
     * @return string
     */
    // public static function get_username($user_id) {
    //     $user = DB::table('users')->where('userid', $user_id)->first();

    //     return (isset($user->username) ? $user->username : '');
    // }

    public static function covtime($youtube_time) {
        preg_match_all('/(\d+)/',$youtube_time,$parts);

        // Put in zeros if we have less than 3 numbers.
        if (count($parts[0]) == 1) {
            array_unshift($parts[0], "0", "0");
        } elseif (count($parts[0]) == 2) {
            array_unshift($parts[0], "0");
        }

        $sec_init = $parts[0][2];
        $seconds = $sec_init%60;
        $seconds_overflow = floor($sec_init/60);

        $min_init = $parts[0][1] + $seconds_overflow;
        $minutes = ($min_init)%60;
        $minutes_overflow = floor(($min_init)/60);

        $hours = $parts[0][0] + $minutes_overflow;
        if(strlen($hours) == 1){
          $hours = '0' . $hours;
        }
        if(strlen($minutes) == 1){
          $minutes = '0' . $minutes;
        }
        if(strlen($seconds) == 1){
          $seconds = '0' . $seconds;
        }
        if($hours != 0)
            return $hours.':'.$minutes.':'.$seconds;
        else
            return $minutes.':'.$seconds;
    }

    public static function covTimeToTr($time = '')
    {
        $outTime='';

        $datetime1 =  date('Y-n-d h:s');
        $datetime2 =  date($time);
        $interval = $datetime1-$datetime2;
        $elapsed = $interval->format('%y years %m months %d days %h hours %i minutes %S seconds');
        $Y=$interval->format('%y');
        $M=$interval->format('%m');
        $D=$interval->format('%d');
        $H=$interval->format('%h');
        $I=$interval->format('%i');
        $S=$interval->format('%S');

        if($Y>0)
          return $outTime.=$Y. __('year');
        if($M>0)
           return $outTime.=$M. __('mounth');
        if($D>0)
           return $outTime.=$D. __('day');
        if($H>0)
          return $outTime.=$H.__('hour').$I . __('minute') ;
        if($I>0)
          return $outTime.=$I.' phút ';
        if($S>0)
          return $outTime.=$S.' giây ';

    }

    public static function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => __('app.year'),
            'm' => __('app.month'),
            'w' => __('app.week'),
            'd' => __('app.day'),
            'h' => __('app.hour'),
            'i' => __('app.minute'),
            's' => __('app.second'),
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v;
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        if($string){
            $string = implode(', ', $string);
            if($string >1 && Session::get('locale') == 'en')
                $string .= 's';
            $return = $string . ' ' . __('app.before'); //: 'vừa xong';
        } else {
            $return = __('app.just_moment');
        }

        return $return;
    }

    public static function convert_vi_to_en($str) {
      $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
      $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
      $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
      $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
      $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
      $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
      $str = preg_replace("/(đ)/", 'd', $str);
      $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
      $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
      $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
      $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
      $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
      $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
      $str = preg_replace("/(Đ)/", 'D', $str);
      $str = str_replace(" ", '-', str_replace("&*#39;","",$str));
      return $str;
    }

    public static function country_code_to_locale($country_code, $language_code = '')
    {
        // Locale list taken from:
        // http://stackoverflow.com/questions/3191664/
        // list-of-all-locales-and-their-short-codes
        $locales = array('af-ZA',
                        'am-ET',
                        'ar-AE',
                        'ar-BH',
                        'ar-DZ',
                        'ar-EG',
                        'ar-IQ',
                        'ar-JO',
                        'ar-KW',
                        'ar-LB',
                        'ar-LY',
                        'ar-MA',
                        'arn-CL',
                        'ar-OM',
                        'ar-QA',
                        'ar-SA',
                        'ar-SY',
                        'ar-TN',
                        'ar-YE',
                        'as-IN',
                        'az-Cyrl-AZ',
                        'az-Latn-AZ',
                        'ba-RU',
                        'be-BY',
                        'bg-BG',
                        'bn-BD',
                        'bn-IN',
                        'bo-CN',
                        'br-FR',
                        'bs-Cyrl-BA',
                        'bs-Latn-BA',
                        'ca-ES',
                        'co-FR',
                        'cs-CZ',
                        'cy-GB',
                        'da-DK',
                        'de-AT',
                        'de-CH',
                        'de-DE',
                        'de-LI',
                        'de-LU',
                        'dsb-DE',
                        'dv-MV',
                        'el-GR',
                        'en-029',
                        'en-AU',
                        'en-BZ',
                        'en-CA',
                        'en-GB',
                        'en-IE',
                        'en-IN',
                        'en-JM',
                        'en-MY',
                        'en-NZ',
                        'en-PH',
                        'en-SG',
                        'en-TT',
                        'en-US',
                        'en-ZA',
                        'en-ZW',
                        'es-AR',
                        'es-BO',
                        'es-CL',
                        'es-CO',
                        'es-CR',
                        'es-DO',
                        'es-EC',
                        'es-ES',
                        'es-GT',
                        'es-HN',
                        'es-MX',
                        'es-NI',
                        'es-PA',
                        'es-PE',
                        'es-PR',
                        'es-PY',
                        'es-SV',
                        'es-US',
                        'es-UY',
                        'es-VE',
                        'et-EE',
                        'eu-ES',
                        'fa-IR',
                        'fi-FI',
                        'fil-PH',
                        'fo-FO',
                        'fr-BE',
                        'fr-CA',
                        'fr-CH',
                        'fr-FR',
                        'fr-LU',
                        'fr-MC',
                        'fy-NL',
                        'ga-IE',
                        'gd-GB',
                        'gl-ES',
                        'gsw-FR',
                        'gu-IN',
                        'ha-Latn-NG',
                        'he-IL',
                        'hi-IN',
                        'hr-BA',
                        'hr-HR',
                        'hsb-DE',
                        'hu-HU',
                        'hy-AM',
                        'id-ID',
                        'ig-NG',
                        'ii-CN',
                        'is-IS',
                        'it-CH',
                        'it-IT',
                        'iu-Cans-CA',
                        'iu-Latn-CA',
                        'ja-JP',
                        'ka-GE',
                        'kk-KZ',
                        'kl-GL',
                        'km-KH',
                        'kn-IN',
                        'kok-IN',
                        'ko-KR',
                        'ky-KG',
                        'lb-LU',
                        'lo-LA',
                        'lt-LT',
                        'lv-LV',
                        'mi-NZ',
                        'mk-MK',
                        'ml-IN',
                        'mn-MN',
                        'mn-Mong-CN',
                        'moh-CA',
                        'mr-IN',
                        'ms-BN',
                        'ms-MY',
                        'mt-MT',
                        'nb-NO',
                        'ne-NP',
                        'nl-BE',
                        'nl-NL',
                        'nn-NO',
                        'nso-ZA',
                        'oc-FR',
                        'or-IN',
                        'pa-IN',
                        'pl-PL',
                        'prs-AF',
                        'ps-AF',
                        'pt-BR',
                        'pt-PT',
                        'qut-GT',
                        'quz-BO',
                        'quz-EC',
                        'quz-PE',
                        'rm-CH',
                        'ro-RO',
                        'ru-RU',
                        'rw-RW',
                        'sah-RU',
                        'sa-IN',
                        'se-FI',
                        'se-NO',
                        'se-SE',
                        'si-LK',
                        'sk-SK',
                        'sl-SI',
                        'sma-NO',
                        'sma-SE',
                        'smj-NO',
                        'smj-SE',
                        'smn-FI',
                        'sms-FI',
                        'sq-AL',
                        'sr-Cyrl-BA',
                        'sr-Cyrl-CS',
                        'sr-Cyrl-ME',
                        'sr-Cyrl-RS',
                        'sr-Latn-BA',
                        'sr-Latn-CS',
                        'sr-Latn-ME',
                        'sr-Latn-RS',
                        'sv-FI',
                        'sv-SE',
                        'sw-KE',
                        'syr-SY',
                        'ta-IN',
                        'te-IN',
                        'tg-Cyrl-TJ',
                        'th-TH',
                        'tk-TM',
                        'tn-ZA',
                        'tr-TR',
                        'tt-RU',
                        'tzm-Latn-DZ',
                        'ug-CN',
                        'uk-UA',
                        'ur-PK',
                        'uz-Cyrl-UZ',
                        'uz-Latn-UZ',
                        'vi-VN',
                        'wo-SN',
                        'xh-ZA',
                        'yo-NG',
                        'zh-CN',
                        'zh-HK',
                        'zh-MO',
                        'zh-SG',
                        'zh-TW',
                        'zu-ZA',);

        foreach ($locales as $locale)
        {
            $array = explode('-', $locale);
            $locale_region = $array[1];
            $locale_language = $array[0];
            $locale_array = array('language' => $locale_language,
                                 'region' => $locale_region);

            if (strtoupper($country_code) == $locale_region &&
                $language_code == '')
            {
                return strtolower($locale_language);
            }
            elseif (strtoupper($country_code) == $locale_region &&
                    strtolower($language_code) == $locale_language)
            {
                return strtolower($locale_language);
            }
        }

        return null;
    }

    public static function COUNTRIES(){
        return [
            // 'AF' => 'Afghanistan',
            // 'AX' => 'Aland Islands',
            // 'AL' => 'Albania',
            'DZ' => 'Algeria',
            // 'AS' => 'American Samoa',
            // 'AD' => 'Andorra',
            // 'AO' => 'Angola',
            // 'AI' => 'Anguilla',
            // 'AQ' => 'Antarctica',
            // 'AG' => 'Antigua And Barbuda',
            'AR' => 'Argentina',
            // 'AM' => 'Armenia',
            // 'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            //'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            //'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            // 'BZ' => 'Belize',
            // 'BJ' => 'Benin',
            // 'BM' => 'Bermuda',
            // 'BT' => 'Bhutan',
            //'BO' => 'Bolivia',
            'BA' => 'Bosnia And Herzegovina',
            // 'BW' => 'Botswana',
            // 'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            // 'IO' => 'British Indian Ocean Territory',
            // 'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgaria',
            // 'BF' => 'Burkina Faso',
            // 'BI' => 'Burundi',
            // 'KH' => 'Cambodia',
            // 'CM' => 'Cameroon',
            'CA' => 'Canada',
            // 'CV' => 'Cape Verde',
            // 'KY' => 'Cayman Islands',
            // 'CF' => 'Central African Republic',
            // 'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            // 'CX' => 'Christmas Island',
            // 'CC' => 'Cocos (Keeling) Islands',
            'CO' => 'Colombia',
            // 'KM' => 'Comoros',
            // 'CG' => 'Congo',
            // 'CD' => 'Congo, Democratic Republic',
            // 'CK' => 'Cook Islands',
            // 'CR' => 'Costa Rica',
            // 'CI' => 'Cote D\'Ivoire',
            'HR' => 'Croatia',
            // 'CU' => 'Cuba',
            // 'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DK' => 'Denmark',
            // 'DJ' => 'Djibouti',
            // 'DM' => 'Dominica',
            // 'DO' => 'Dominican Republic',
            // 'EC' => 'Ecuador',
            // 'EG' => 'Egypt',
            // 'SV' => 'El Salvador',
            // 'GQ' => 'Equatorial Guinea',
            // 'ER' => 'Eritrea',
            'EE' => 'Estonia',
            // 'ET' => 'Ethiopia',
            // 'FK' => 'Falkland Islands (Malvinas)',
            // 'FO' => 'Faroe Islands',
            // 'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            // 'GF' => 'French Guiana',
            // 'PF' => 'French Polynesia',
            // 'TF' => 'French Southern Territories',
            // 'GA' => 'Gabon',
            // 'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            // 'GI' => 'Gibraltar',
            // 'GR' => 'Greece',
            // 'GL' => 'Greenland',
            // 'GD' => 'Grenada',
            // 'GP' => 'Guadeloupe',
            // 'GU' => 'Guam',
            // 'GT' => 'Guatemala',
            // 'GG' => 'Guernsey',
            // 'GN' => 'Guinea',
            // 'GW' => 'Guinea-Bissau',
            // 'GY' => 'Guyana',
            'HT' => 'Haiti',
            //'HM' => 'Heard Island & Mcdonald Islands',
            //'VA' => 'Holy See (Vatican City State)',
            //'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran, Islamic Republic Of',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IM' => 'Isle Of Man',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            //'JE' => 'Jersey',
            //'JO' => 'Jordan',
            //'KZ' => 'Kazakhstan',
            //'KE' => 'Kenya',
            //'KI' => 'Kiribati',
            'KR' => 'Korea',
            'KW' => 'Kuwait',
            //'KG' => 'Kyrgyzstan',
            'LA' => 'Lao People\'s Democratic Republic',
            //'LV' => 'Latvia',
            //'LB' => 'Lebanon',
            //'LS' => 'Lesotho',
            'LR' => 'Liberia',
            //'LY' => 'Libyan Arab Jamahiriya',
            //'LI' => 'Liechtenstein',
            //'LT' => 'Lithuania',
            //'LU' => 'Luxembourg',
            //'MO' => 'Macao',
            //'MK' => 'Macedonia',
            //'MG' => 'Madagascar',
            //'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            //'ML' => 'Mali',
            //'MT' => 'Malta',
            //'MH' => 'Marshall Islands',
            //'MQ' => 'Martinique',
            //'MR' => 'Mauritania',
            //'MU' => 'Mauritius',
            //'YT' => 'Mayotte',
            'MX' => 'Mexico',
            //'FM' => 'Micronesia, Federated States Of',
            //'MD' => 'Moldova',
            'MC' => 'Monaco',
            //'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            //'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            //'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'AN' => 'Netherlands Antilles',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            //'NG' => 'Nigeria',
            //'NU' => 'Niue',
            //'NF' => 'Norfolk Island',
            //'MP' => 'Northern Mariana Islands',
            //'NO' => 'Norway',
            //'OM' => 'Oman',
            'PK' => 'Pakistan',
            //'PW' => 'Palau',
            //'PS' => 'Palestinian Territory, Occupied',
            'PA' => 'Panama',
            //'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            //'PN' => 'Pitcairn',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            //'RE' => 'Reunion',
            'RO' => 'Romania',
            'RU' => 'Russian Federation',
            //'RW' => 'Rwanda',
            //'BL' => 'Saint Barthelemy',
            'SH' => 'Saint Helena',
            //'KN' => 'Saint Kitts And Nevis',
            //'LC' => 'Saint Lucia',
            //'MF' => 'Saint Martin',
            //'PM' => 'Saint Pierre And Miquelon',
            //'VC' => 'Saint Vincent And Grenadines',
            //'WS' => 'Samoa',
            //'SM' => 'San Marino',
            'ST' => 'Sao Tome And Principe',
            'SA' => 'Saudi Arabia',
            //'SN' => 'Senegal',
            'RS' => 'Serbia',
            'SC' => 'Seychelles',
            //'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SK' => 'Slovakia',
            //'SI' => 'Slovenia',
            //'SB' => 'Solomon Islands',
            //'SO' => 'Somalia',
           // 'ZA' => 'South Africa',
            //'GS' => 'South Georgia And Sandwich Isl.',
            'ES' => 'Spain',
            //'LK' => 'Sri Lanka',
            //'SD' => 'Sudan',
            //'SR' => 'Suriname',
            //'SJ' => 'Svalbard And Jan Mayen',
            //'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            //'SY' => 'Syrian Arab Republic',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania',
            'TH' => 'Thailand',
            //'TL' => 'Timor-Leste',
            //'TG' => 'Togo',
            //'TK' => 'Tokelau',
            //'TO' => 'Tonga',
            //'TT' => 'Trinidad And Tobago',
            //'TN' => 'Tunisia',
            'TR' => 'Turkey',
            //'TM' => 'Turkmenistan',
            //'TC' => 'Turks And Caicos Islands',
            'TV' => 'Tuvalu',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'US' => 'United States',
            'UM' => 'United States Outlying Islands',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VE' => 'Venezuela',
            'VN' => 'Viet Nam',
            'VG' => 'Virgin Islands, British',
            'VI' => 'Virgin Islands, U.S.',
            'WF' => 'Wallis And Futuna',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe',
        ];
    }
}
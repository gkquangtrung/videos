<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Session;
use App\Helpers\General;
use Torann\GeoIP\Facades\GeoIP;

class Locale
{
    public function handle($request, Closure $next)
    {
        if ( !Session::has('location')) {
            $Location = GeoIP::getLocation($request->ip());
            $lang = General::country_code_to_locale($Location->iso_code);
            Session::put('location', $Location);

            if ( !Session::has('locale')) {
                if ($lang) {
                  Session::put('locale', $lang);
                } else {
                  Session::put('locale', config('app.locale'));
                }
            }
        }

        App::setLocale(Session::has('locale') ? Session::get('locale') : Config::get('app.locale'));
        return $next($request);
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alaouy\Youtube\Facades\Youtube;
use App\Helpers\General;
use App\video;
use App\channel;

class VideoController extends Controller
{
    public function show(Request $request)
    {
        $video = video::where('video_id', $request->id)->first();
        if (!$video) {
            $video = Youtube::getVideoInfo($request->id);
            if (empty($video)) {
                abort(404);
            } else {
                $video->player->embedHtml = str_replace('<iframe',  '<iframe id="ytplayer"', $video->player->embedHtml);
                $video->player->embedHtml = str_replace($request->id, $request->id . '?autoplay=1&enablejsapi=1', $video->player->embedHtml);
                //var_dump($video->snippet->description);die;
                $dataVideo = array(
                    'video_id'      => $video->id,
                    'channel_id'    => $video->snippet->channelId,
                    'title'         => $video->snippet->title,
                    'img_default'   => $video->snippet->thumbnails->default->url,
                    'img_high'      => $video->snippet->thumbnails->high->url,
                    'img_standard'  => $video->snippet->thumbnails->standard->url,
                    'img_maxres'    => $video->snippet->thumbnails->maxres->url,
                    'embed_html'    => $video->player->embedHtml,
                    'view_count'    => $video->statistics->viewCount,
                    'dislike_count' => $video->statistics->dislikeCount,
                    'like_count'    => $video->statistics->likeCount,
                    'description'   => utf8_encode($video->snippet->description),
                    'created_date'  => date('Y-m-d'),
                    'modified_date' => date('Y-m-d'),
                );
                //dd($dataVideo);
                video::insert($dataVideo);
                $video = (object)$dataVideo;

            }
        }

        $channel = channel::where('channel_id', $video->channel_id)->first();
        if (!$channel) {
            $channel = Youtube::getChannelById($video->channel_id);//dd($channel);
            $dataChannel = array(
                'channel_id' => $channel->id,
                'title' => $channel->snippet->title,
                'description' => $channel->snippet->description,
                'published_at' => $channel->snippet->publishedAt,
                'img_default' => $channel->snippet->thumbnails->default->url,
                'img_medium' => $channel->snippet->thumbnails->medium->url,
                'img_high' => $channel->snippet->thumbnails->high->url,
                'subscriber_count' => $channel->statistics->subscriberCount,
                'video_count' => $channel->statistics->videoCount,
                'created_date'  => date('Y-m-d'),
                'modified_date' => date('Y-m-d'),
            );

            channel::insert($dataChannel);
            $channel = (object)$dataChannel;
        }

        $relatedVideos = Youtube::getRelatedVideos($request->id);

        $dataAssign = [
            'ogTitle'        => $video->title,
            'ogDescription'  => 'xem video chọn lọc miễn phí...',
            'ogImage'        => $video->img_high,
            'video'          => $video,
            'channel'        => $channel,
            'relatedVideos'  => $relatedVideos,
        ];

        return view('video.show', $dataAssign);
    }

    public function search(Request $request)
    {
        $key = $request->input('key');

        $videoList = Youtube::searchVideos($key,50,null,['id', 'snippet']);

        $dataAssign = [
            'ogTitle'       => 'Tìm kiếm video',
            'ogDescription' => 'Tìm kiếm video',
            'ogImage'       => '',
            'videoList'    => $videoList,
        ];

        return view('video.search', $dataAssign);
    }

    public function trending(Request $request)
    {
        $contries = General::COUNTRIES();
        $contryCode = $request->contryCode;
        if (!$contryCode || strlen($contryCode) != 2) {
            abort(404);
        }
        $videoList = Youtube::getPopularVideos($contryCode, 48);

        if ($videoList == NULL) {
            abort(404);
        }

        $dataAssign = [
            'ogTitle'       => 'Video nổi bật',
            'ogDescription' => 'xem video nổi bất nhất Việt Nam...',
            'ogImage'       => '',
            'videoList'     => $videoList,
            'contries'      => $contries,
        ];

        return view('video.trending', $dataAssign);
    }

    public function live(Request $request)
    {
        $contryCode = $request->contryCode;

        $videoList = Youtube::searchLiveVideos('tin tức', 50, 'date');

        $dataAssign = [
            'ogTitle'       => 'Video live',
            'ogDescription' => 'xem video chọn lọc miễn phí...',
            'ogImage'       => '',
            'videoList'     => $videoList,
        ];

        return view('video.live', $dataAssign);
    }

    public function getCategory(Request $request)
    {
        $categoryName = $request->categoryName;
        switch ($categoryName) {
            case 'movies':
                $id = 'PLHPTxTxtC0iYvH39VS4pz9apTOYPrXsGV';
                break;
            case 'music':
                $id = 'PLFgquLnL59anfy66Thi2waVYeMxbfgHr1';
                break;
            case 'game':
                $id = 'PLiCvVJzBupKnKoAJR3T8NxXwA5mPeBD8W';
                break;
            default:
                abort(404);
                break;
        }

        $videoList = Youtube::getPlaylistItemsByPlaylistId($id);
        //dd($videoList);
        $dataAssign = [
            'ogTitle'       => 'Kênh video',
            'ogDescription' => 'xem video chọn lọc miễn phí...',
            'ogImage'       => '',
            'videoList'     => $videoList['results'],
        ];

        return view('video.category', $dataAssign);
    }

    public function getVideoByChannelId(Request $request)
    {
        $channelId = $request->channelId;
        $videoList = Youtube::listChannelVideos($channelId, 50, 'date');
        //dd($videoList);

        $dataAssign = [
            'ogTitle'       => 'Video live',
            'ogDescription' => 'xem video chọn lọc miễn phí...',
            'ogImage'       => '',
            'videoList'     => $videoList,
        ];

        return view('video.channel', $dataAssign);
    }
}

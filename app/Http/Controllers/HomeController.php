<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alaouy\Youtube\Facades\Youtube;
use Stevebauman\Location\Facades\Location;
use App;
use App\video;

class HomeController extends Controller
{
    public function index()
    {
        $position = Location::get();

        $videoList = Youtube::getPopularVideos($position->iso_code, 18);

        $dataAssign = [
            'ogTitle'       => 'Kênh video tổng hợp - tube102.com',
            'ogDescription' => 'tube102.com là Kênh Video Giải Trí Tổng Hợp miễn phí hay nhất Việt Nam. Được cập nhật liên tục những video mới và nổi bật. Kênh video giải trí tuyệt vời nhất Việt Nam.',
            'ogImage'       => asset('templates/video-post/img/play.png'),
            'videoList'     => $videoList,
        ];

        switch ($position->iso_code) {
            case 'VN':
                $dataAssign['fapTVVideos'] = Youtube::listChannelVideos('UC0jDoh3tVXCaqJ6oTve8ebA', 12, 'date');
                $dataAssign['list60sVideos'] = Youtube::listChannelVideos('UCRjzfa1E0gA50lvDQipbDMg', 12, 'date');
                $dataAssign['POPSMUSIC'] = Youtube::listChannelVideos('UCUgXK2UjZ8G_EM438aYkGrw', 12, 'date');
                $dataAssign['HTVEntertainment'] = Youtube::listChannelVideos('UCbq8aOyj9ZtIqcD-0MwG1fQ', 12, 'date');
                $dataAssign['remixMusic'] = Youtube::listChannelVideos('UCJTrwkBVzz6sR_X5PoQ2VXg', 12, 'date');
                break;
            case 'US':
                $dataAssign['RyanToysReview'] = Youtube::listChannelVideos('UChGJGhZ9SOOHvBB0Y4DOO_w', 12, 'date');
                $dataAssign['Movieclips'] = Youtube::listChannelVideos('UC3gNmTGu-TTbFPpfSs5kNkg', 12, 'date');
                $dataAssign['WWE'] = Youtube::listChannelVideos('UCJ5v_MCY6GNUBTO8-D3XoAg', 12, 'date');
                $dataAssign['WORLDSTARHIPHOP'] = Youtube::listChannelVideos('UC-yXuc1__OzjwpsJPlxYUCQ', 12, 'date');
                break;

            default:
                break;
        }

        return view('index', $dataAssign);
    }

    public function sitemap() {
         // create new sitemap object
        $sitemap = App::make("sitemap");

        // add items to the sitemap (url, date, priority, freq)
        $sitemap->add(url('/'), date('Y-m-dTH:i:sP', time()), '1.0', 'daily');
        $sitemap->add(url('/live'), date('Y-m-dTH:i:sP', time()), '0.9', 'daily');
        $sitemap->add(url('/trending/vn'), date('Y-m-dTH:i:sP', time()), '0.9', 'daily');
        $sitemap->add(url('/trending/us'), date('Y-m-dTH:i:sP', time()), '0.9', 'daily');
        $sitemap->add(url('/trending/fr'), date('Y-m-dTH:i:sP', time()), '0.9', 'daily');
        //$sitemap->add(url('/page'), '2012-08-26T12:30:00+02:00', '0.9', 'monthly');

        // get all posts from db
        $videos = video::all();

        //add every post to the sitemap
        foreach ($videos as $video)
        {
            $sitemap->add(route('video.show',['id'=>$video->video_id]), date('Y-m-dTH:i:sP', strtotime($video->modified_date)), '0.8', 'daily');
        }

        // generate your sitemap (format, filename)
        $sitemap->store('xml', 'sitemap');
        // this will generate file mysitemap.xml to your public folder
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Laravel PHP Facade/Wrapper for the Youtube Data API v3
|--------------------------------------------------------------------------
|
| Here is where you can set your key for Youtube API. In case you do not
| have it, it can be acquired from: https://console.developers.google.com
*/
if (date('H') < 12){
  $key = env('YOUTUBE_API_KEY');
} else {
  $key = env('YOUTUBE_API_KEY2');
}

return [
    'key' => $key
];

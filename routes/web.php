<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 Route::group(['middleware' => 'localization', 'prefix' => Session::get('locale')], function() {

    Route::get('/home', 'HomeController@index');

    Route::post('/lang', [
        'as' => 'switchLang',
        'uses' => 'LangController@postLang',
    ]);

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/video/{id}', 'VideoController@show')->name('video.show');
    Route::get('/trending/{contryCode}', 'VideoController@trending')->name('video.trending');
    Route::get('/live', 'VideoController@live')->name('video.live');
    Route::post('/search', 'VideoController@search')->name('video.search');
    Route::get('/category/{categoryName}', 'VideoController@getCategory')->name('category');
    Route::get('/channel/{channelId}', 'VideoController@getVideoByChannelId')->name('channel');

    Route::get('/test', 'testController@index')->name('test');
    Route::get('/sitemap', 'HomeController@sitemap')->name('sitemap');
    Route::get('/mysitemap', function(){

        // create new sitemap object
        $sitemap = App::make("sitemap");

        // add items to the sitemap (url, date, priority, freq)
        $sitemap->add(url('/'), '2012-08-25T20:10:00+02:00', '1.0', 'daily');
        $sitemap->add(url('/page'), '2012-08-26T12:30:00+02:00', '0.9', 'monthly');

        // get all posts from db
        //$posts = DB::table('posts')->orderBy('created_at', 'desc')->get();

        // add every post to the sitemap
        // foreach ($posts as $post)
        // {
            //$sitemap->add('link', $post->modified, $post->priority, $post->freq);
            $sitemap->add('link2', '2012-08-26T12:30:00+02:00', '0.9', 'monthly');
        //}

        // generate your sitemap (format, filename)
        $sitemap->store('xml', 'mysitemap');
        // this will generate file mysitemap.xml to your public folder

    });



    Route::get('/{any}', function(){ abort(404);})->name('404');


  });




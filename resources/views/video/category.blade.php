@extends('layouts.home.index', ['title' => 'Video phát trực tiếp - tube102.com'])

@section('content')
<div class="site-output">
   <!-- // col-md-2 -->
   <div id="all-output" class="col-md-12">
      <h1 class="new-video-title"><i class="fa fa-life-ring"></i> Videos {{ request()->categoryName }}: <label style="color: #d41a2e;text-transform: none;">{{ request()->input('key') }}</label></h1>
      <div class="row">
         @if($videoList)
           @foreach($videoList as $video)
            <!-- video-item -->
            @include('partials.video-item-search', ['video' => $video])
            <!-- // video-item -->
           @endforeach
         @endif
      </div>
   </div>
</div>
@endsection
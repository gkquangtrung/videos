﻿@extends('layouts.home.index', ['title' => 'Tìm kiếm'])

@section('content')
<div class="site-output">
   <!-- // col-md-2 -->
   <div id="all-output" class="col-md-12">
      <h1 class="new-video-title"><i class="fa fa-bolt"></i> Tìm kiếm cho: <label style="color: #d41a2e;text-transform: none;">{{ request()->input('key') }}</label></h1>
      <div class="row">
        @foreach($videoList as $video)
         <!-- video-item -->
         @include('partials.video-item-channel', ['video' => $video])
         <!-- // video-item -->
        @endforeach
      </div>
   </div>
</div>
@endsection
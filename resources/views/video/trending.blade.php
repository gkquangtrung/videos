@extends('layouts.home.index', ['title' => 'Video phổ biến - tube102.com'])

@section('content')
<div class="site-output">
   <!-- // col-md-2 -->
   <div id="all-output" class="col-md-12">
      <h1 class="new-video-title"><i class="fa fa-bolt"></i> Videos trending for:
         <a href="javascript:void(0);" data-toggle="collapse" data-target="#contries">
            <label style="color: #d41a2e;text-transform: none; cursor: pointer;">{{ $contries[strtoupper(request()->contryCode)] }} <i style="vertical-align: middle;font-weight: bold;" class="fa fa-angle-down"></i>
            </label>
         </a>
      </h1>
      <div class="row">
         <div id="contries" class="collapse">
            <div class="f16">
               <link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/2.9.0/css/flag-icon.css" rel="stylesheet">
               @foreach($contries  as $contryCode => $contry)
                  <div class="country_code ">
                     <a href="{{ url('trending/'.strtolower($contryCode)) }}">
                        <span class="flag-icon flag-icon-{{strtolower($contryCode)}}"></span> {{ $contry }}
                     </a>
                  </div>
               @endforeach
            </div>
         </div>
      </div>

      <div class="row">
        @foreach($videoList as $video)
         <!-- video-item -->
         @include('partials.video-item', ['video' => $video])
         <!-- // video-item -->
        @endforeach
      </div>
   </div>
</div>
@endsection
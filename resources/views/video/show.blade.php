@extends('layouts.home.index', ['title' => $video->title])
@section('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.12&appId=1770363203221011&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<div class="site-output">
   <div id="all-output" class="col-md-12">
      <div class="row">
         <!-- Watch -->
         <div class="col-md-9">
            <div id="watch">
               <!-- Video Player -->
               <div class="video-code">
                  {!! $video->embed_html !!}
               </div>
               <!-- // video-code -->
               <div class="video-share">
                  <h1 class="video-title">{{ $video->title }}</h1>

                  <!-- Chanels Item -->
               <div class="chanel-item">
                  <div class="chanel-thumb">
                     <a href="#"><img src="{{ $channel->img_default }}" alt=""></a>
                  </div>
                  <div class="chanel-info">
                     <a class="title" href="#">{{ $channel->title }}</a>
                     <span class="subscribers">{{ number_format($channel->subscriber_count) }} subscribers</span>
                  </div>
                  <div style="float: right">
                    <p class="views">
                      <a  href="javascript:void(0);">{{ number_format($video->view_count) }}
                         {{ __('app.views') }}
                      </a>
                    </p>
                    <ul class="like">
                      <li>
                        <a class="like" href="javascript:void(0);">{{ number_format($video->dislike_count) }}
                          <i class="fa fa-thumbs-up"></i>
                        </a>
                      </li>
                      <li>
                        <a class="deslike" href="javascript:void(0);">{{ number_format($video->like_count) }}
                          <i class="fa fa-thumbs-down"></i>
                        </a>
                      </li>
                    </ul>

                  </div>
                  <ul class="social_link">
                     <li><a class="facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                     <li><a class="linkedin" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                     <li><a class="google" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                     <li><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                  </ul>

               </div>
               <!-- // Chanels Item -->
               </div>
               <!-- // video-share -->
               <!-- // Video Player -->

               <div class="">
                <span class="more">
                  {!! nl2br(utf8_decode($video->description)) !!}
                </span>
               </div>
               <!-- Comments -->
               <div class="fb-comments" data-href="{{ url()->current() }}" data-width="100%" data-numposts="10"></div>
               <!-- end Comments -->
            </div>
            <!-- // watch -->
         </div>
         <!-- // col-md-8 -->
         <!-- // Watch -->
         <!-- Related Posts-->
         <div class="col-md-3">


            <div id="related-posts">
              <!-- Slide THREE -->
              <div class="autoPlayNextBox">
                <div class="pull-left">
                  <label class="labNext">Next</label>
                </div>

                <div class="slideThree pull-right">
                  <input type="checkbox" value="None" id="slideAutoNext" name="check" checked />
                  <label for="slideAutoNext"></label>
                </div>
                <div class="pull-right">
                  <label class="labAutoplay">Autoplay: </label>
                </div>
                <div class="clearfix"></div>
              </div>

               @foreach($relatedVideos as $relatedVideo)
                  <!-- video item -->
                  <div class="related-video-item">
                     <div class="thumb">
                        <a href="{{ route('video.show',['id'=>$relatedVideo->id->videoId]) }}"><img src="{{ $relatedVideo->snippet->thumbnails->medium->url }}" alt=""></a>
                     </div>
                     <a href="{{ route('video.show',['id'=>$relatedVideo->id->videoId]) }}" class="title" title="{{ $relatedVideo->snippet->title }}">{{ $relatedVideo->snippet->title }}</a>
                     <a class="channel-name" href="#">{{ $relatedVideo->snippet->channelTitle }}<span>
                     <br>
                     <span class="date" style="font-size: 10px"><i class="fa fa-clock-o"></i> {{ General::time_elapsed_string($relatedVideo->snippet->publishedAt) }} </span>
                  </div>
                  <!-- // video item -->
               @endforeach
            </div>
         </div>
         <!-- // col-md-4 -->
         <!-- // Related Posts -->
      </div>
      <!-- // row -->
   </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 300;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Show more >";
    var lesstext = "Show less";

    $('.more').each(function() {
        var content = $(this).html();
        if(content.length > showChar) {
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
            $(this).html(html);
        }
    });

    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});
</script>
<script type="text/javascript">
  var player;
  var cookieAutoNext = readCookie('autoNext');

  if (cookieAutoNext == 0) {
    $('#slideAutoNext').prop('checked', false);
  }

  function onYouTubePlayerAPIReady() {
    player = new YT.Player('ytplayer', {
      events: {
          'onStateChange': ShowMe
         }
    });
  }

  var tag = document.createElement('script');
  tag.src = "http://www.youtube.com/player_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  function ShowMe() {
    var sStatus;
    sStatus = player.getPlayerState();

    if (sStatus == 0){
      if ($('#slideAutoNext').is(":checked")){
        window.location.replace("{{ route('video.show',['id'=>$relatedVideos[0]->id->videoId]) }}");
      }
    }
  }

  function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
  }

  $("#slideAutoNext").change(function() {
    if ($(this).is(":checked")){
      document.cookie = "autoNext=1";
    } else {
      document.cookie = "autoNext=0";
    }
  });

</script>
@endsection
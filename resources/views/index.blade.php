@extends('layouts.home.index', ['title' => 'Kênh video giải trí tổng hợp Việt Nam - tube102.com'])

@section('content')
<div class="site-output">
   <!-- // col-md-2 -->
   <div id="all-output" class="col-md-12">
      <h1 class="new-video-title"><i class="fa fa-video-camera"></i> Tube102.com - {{ __('app.featured_videos') }}</h1>
      <div class="row">
        @foreach($videoList as $video)
         <!-- video-item -->
         @include('partials.video-item', ['video' => $video])
         <!-- // video-item -->
        @endforeach
      </div>
      @if(Session::get('location')->iso_code == 'VN')
        <h2 class="new-video-title"><i class="fa fa-video-camera"></i> FAPtv</h2>
        <div class="row">
          @foreach($fapTVVideos as $video)
           <!-- video-item -->
           @include('partials.video-item-channel', ['video' => $video])
           <!-- // video-item -->
          @endforeach
          <div class="col-xs-12">
            <a class="btn btn-view-more " href="{{ route('channel', ['channelId'=>$video->snippet->channelId]) }}">Xem thêm video FAPtv</a>
          </div>
        </div>

        <h2 class="new-video-title"><i class="fa fa-video-camera"></i> HTV Entertainment</h2>
        <div class="row">
          @foreach($HTVEntertainment as $video)
           <!-- video-item -->
           @include('partials.video-item-channel', ['video' => $video])
           <!-- // video-item -->
          @endforeach
          <div class="col-xs-12">
            <a class="btn btn-view-more " href="{{ route('channel', ['channelId'=>$video->snippet->channelId]) }}">Xem thêm video HTV Entertainment</a>
          </div>
        </div>

        <h2 class="new-video-title"><i class="fa fa-video-camera"></i> 60 Giây Official</h2>
        <div class="row">
          @foreach($list60sVideos as $video)
           <!-- video-item -->
           @include('partials.video-item-channel', ['video' => $video])
           <!-- // video-item -->
          @endforeach
          <div class="col-xs-12">
            <a class="btn btn-view-more " href="{{ route('channel', ['channelId'=>$video->snippet->channelId]) }}">Xem thêm video 60 Giây Official</a>
          </div>
        </div>

        <h2 class="new-video-title"><i class="fa fa-video-camera"></i> POPSMUSIC</h2>
        <div class="row">
          @foreach($POPSMUSIC as $video)
           <!-- video-item -->
           @include('partials.video-item-channel', ['video' => $video])
           <!-- // video-item -->
          @endforeach
          <div class="col-xs-12">
            <a class="btn btn-view-more " href="{{ route('channel', ['channelId'=>$video->snippet->channelId]) }}">Xem thêm video POPSMUSIC</a>
          </div>
        </div>

        <h2 class="new-video-title"><i class="fa fa-video-camera"></i> Nhạc Remix - Gái Xinh</h2>
        <div class="row">
          @foreach($remixMusic as $video)
           <!-- video-item -->
           @include('partials.video-item-channel', ['video' => $video])
           <!-- // video-item -->
          @endforeach
          <div class="col-xs-12">
            <a class="btn btn-view-more " href="{{ route('channel', ['channelId'=>$video->snippet->channelId]) }}">Xem thêm video Nhạc Remix - Gái Xinh</a>
          </div>
        </div>

        <div class="col-xs-12">
          <div class="intro">
          <p><strong>tube102.com</strong> là <strong>kênh video</strong> giải trí tổng hợp với những <strong>video</strong> được chọn lọc cao đáp ứng cho nhu cầu giải trí của người dùng.</p>
          </div>
        </div>
      @endif

      @if(Session::get('location')->iso_code == 'US')
        <h2 class="new-video-title"><i class="fa fa-video-camera"></i> Movieclips</h2>
        <div class="row">
          @foreach($Movieclips as $video)
           <!-- video-item -->
           @include('partials.video-item-channel', ['video' => $video])
           <!-- // video-item -->
          @endforeach
          <div class="col-xs-12">
            <a class="btn btn-view-more " href="{{ route('channel', ['channelId'=>$video->snippet->channelId]) }}">View more videos for Movieclips</a>
          </div>
        </div>

        <h2 class="new-video-title"><i class="fa fa-video-camera"></i> WWE</h2>
        <div class="row">
          @foreach($WWE as $video)
           <!-- video-item -->
           @include('partials.video-item-channel', ['video' => $video])
           <!-- // video-item -->
          @endforeach
          <div class="col-xs-12">
            <a class="btn btn-view-more " href="{{ route('channel', ['channelId'=>$video->snippet->channelId]) }}">View more videos for WWE</a>
          </div>
        </div>

        <h2 class="new-video-title"><i class="fa fa-video-camera"></i> Ryan ToysReview</h2>
        <div class="row">
          @foreach($RyanToysReview as $video)
           <!-- video-item -->
           @include('partials.video-item-channel', ['video' => $video])
           <!-- // video-item -->
          @endforeach
          <div class="col-xs-12">
            <a class="btn btn-view-more " href="{{ route('channel', ['channelId'=>$video->snippet->channelId]) }}">View more videos for Ryan ToysReview</a>
          </div>
        </div>

        <h2 class="new-video-title"><i class="fa fa-video-camera"></i> WORLDSTARHIPHOP</h2>
        <div class="row">
          @foreach($WORLDSTARHIPHOP as $video)
           <!-- video-item -->
           @include('partials.video-item-channel', ['video' => $video])
           <!-- // video-item -->
          @endforeach
          <div class="col-xs-12">
            <a class="btn btn-view-more " href="{{ route('channel', ['channelId'=>$video->snippet->channelId]) }}">View more videos for WORLDSTARHIPHOP</a>
          </div>
        </div>

        <div class="col-xs-12">
          <div class="intro">
          <p><strong>tube102.com</strong> is an integrated entertainment <strong>video channel</strong> with highly selective <strong>videos</strong> that cater to your entertainment needs.</p>
          </div>
        </div>
      @endif

   </div>
</div>
@endsection
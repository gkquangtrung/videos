<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
   <div class="video-item">
      <div class="thumb">
         <div class="hover-efect"></div>
         <small class="time">{{ General::covtime($video->contentDetails->duration) }}</small>
         <a href="{{ route('video.show',['id' => $video->id]) }}"><img data-src="{{ $video->snippet->thumbnails->medium->url }}" alt=""></a>
      </div>
      <div class="video-info">
         <h3 class="mg0">
            <a href="{{ route('video.show',['id' => $video->id]) }}" class="title" title="{{ $video->snippet->title }}">{{ $video->snippet->title }}</a>
         </h3>
         <a class="channel-name" href="{{ route('channel', ['channelId'=>$video->snippet->channelId]) }}">{{ $video->snippet->channelTitle }}</a>
         <span class="views"><i class="fa fa-eye"></i>{{ number_format($video->statistics->viewCount) }} {{ __('app.views') }} </span>
         <span class="date"><i class="fa fa-clock-o"></i>{{ General::time_elapsed_string($video->snippet->publishedAt) }} </span>
      </div>
   </div>
</div>
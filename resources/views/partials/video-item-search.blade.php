<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
   <div class="video-item">
      <div class="thumb">
         <div class="hover-efect"></div>

         <a href="{{ route('video.show',['id' => $video->contentDetails->videoId ]) }}"><img data-src="{{ isset($video->snippet->thumbnails->medium->url)?$video->snippet->thumbnails->medium->url:'' }}" alt="{{ $video->snippet->title }}"></a>
      </div>
      <div class="video-info">
         <h3 class="mg0">
            <a href="{{ route('video.show',['id' => $video->contentDetails->videoId ]) }}" class="title" title="{{ $video->snippet->title }}">{{ $video->snippet->title }}</a>
         </h3>
         <a class="channel-name" href="#">{{ $video->snippet->channelTitle }}</a>
         <span class="date"><i class="fa fa-clock-o"></i>{{ General::time_elapsed_string($video->snippet->publishedAt) }} </span>
      </div>
   </div>
</div>
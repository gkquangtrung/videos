<!DOCTYPE html>
<html lang="{{ Session::get('locale') }}">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>{{$title}}</title>

      <meta name="description" content="{{ $ogDescription ?? '' }}">
      <meta name="keywords" content="tube102, video hay nhất, video hot, video hai, video được xem nhiều nhất, clip hot nhất, tube102.com" />
      <meta name="author" content="tube102">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="google-site-verification" content="lwjr_b2TotSXoKW3xU-WZEwFmKVxsOGINAuHndqZlnc" />
      <meta property="og:url" content="{{ $ogUrl ?? '' }}"/>
      <meta property="og:type" content="website"/>
      <meta property="og:title" content="{{ $ogTitle ?? '' }}"/>
      <meta property="og:description" content="{{ $ogDescription ?? '' }}"/>
      <meta property="og:image" content="{{ $ogImage ?? '' }}"/>
      <meta property="og:image:width" content="300"/>
      <meta property="og:image:height" content="300"/>
      <!-- Bootstrap Core CSS -->
      <link rel="shortcut icon" href="{{ asset('templates/video-post/img/play.png') }}" type="image/x-icon" >
      <link href="{{ asset('templates/video-post/css/bootstrap.min.css') }}" rel="stylesheet">
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"  type="text/css" />

      <!--Google Fonts-->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800|Raleway:400,500,700|Roboto:300,400,500,700,900|Ubuntu:300,300i,400,400i,500,500i,700" rel="stylesheet">
      <!-- Main CSS -->
      <link rel="stylesheet" href="{{ asset('templates/video-post/css/style.css') }}" />
      <link rel="stylesheet" href="{{ asset('templates/video-post/css/custom.css') }}" />
      <!-- Responsive CSS -->
      <link rel="stylesheet" href="{{ asset('templates/video-post/css/responsive.css') }}" />
      <!-- HTML5 Shim and Respond.js') }} IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js') }} doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}"></script>
      <script src="https://oss.maxcdn.com/libs/respond.{{ asset('templates/video-post/js/1.4.2/respond.min.js') }}"></script>
      <![endif]-->

      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script src="{{ asset('templates/video-post/js/jquery-3.2.1.min.js') }}"></script>
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114520204-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-114520204-1');
      </script>

   </head>
   <body>
      <!--======= header =======-->
      <header>
         <div class="container-full">
            <div class="row">
               <div class="col-lg-2 col-md-2 col-sm-12">

                  <div id="logo" style="text-align: center;margin-left: -20px">
                    <div class="toggle-wrap">
                      <span class="toggle-bar"></span>
                    </div>
                     <a href="{{ url('/') }}"><img src="{{ asset('templates/video-post/img/logo2.png') }}" alt=""></a>
                  </div>
                  <ul class="top-menu hidden-lg hidden-md" style="padding-top: 0; padding-bottom: 10px">
                     <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{ __('app.home') }}</a></li>
                     <li><a href="{{ route('video.trending',['contryCode'=> Session::get('location')->iso_code]) }}"><i class="fa fa-bolt"></i> {{ __('app.trending') }}</a></li>
                     <li><a href="{{ route('video.live') }}"><i class="fa fa-life-ring"></i> Live</a></li>
                  </ul>
               </div>
               <!-- // col-md-2 -->
               <div class="col-lg-6 col-md-6 col-sm-6 ">
                  <div class="search-form" >
                     <form id="search" action="{{ route('video.search') }}" method="post">
                        {{ csrf_field() }}
                        <input type="text" name="key" value="{{ request()->input('key') }}" placeholder="Search any video here..." autocomplete="off" />
                        <input type="submit" value="Keywords"/>
                     </form>
                     <div class="result"></div>
                  </div>
               </div>
               <!-- // col-md-3 -->
               <div class="col-lg-3 col-md-3 col-sm-5 hidden-xs hidden-sm">
                  <ul class="top-menu">
                     <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> {{ __('app.home') }}</a></li>
                     <li><a href="{{ route('video.trending',['contryCode'=> strtolower(Session::get('location')->iso_code)]) }}"><i class="fa fa-bolt"></i> {{ __('app.trending') }}</a></li>
                     <li><a href="{{ route('video.live') }}"><i class="fa fa-life-ring"></i> Live</a></li>
                  </ul>
               </div>
               <!-- // col-md-4 -->
               <div class="col-lg-1 col-md-1 col-sm-3 hidden-xs hidden-sm">
                  <div class="dropdown">
                     <a data-toggle="dropdown" href="#" class="user-area">
                        <div class="thumb"><img src="{{ asset('templates/video-post/demo_img/user-1.png') }}" alt=""></div>
                     </a>
                  </div>
               </div>
            </div>
            <!-- // row -->
         </div>
         <!-- // container-full -->
      </header>

      <aside>
        <div class="left-sidebar">
            <div id="sidebar-stick" >
            <ul class="menu-sidebar">
                <li><a href="{{ url('/') }}"><i class="fa fa-home"></i>{{ __('app.home') }}</a></li>
                <li><a href="{{ route('video.trending',['contryCode'=> Session::get('location')->iso_code]) }}"><i class="fa fa-bolt"></i>{{ __('app.trending') }}</a></li>
                <li><a href="{{ route('video.live') }}"><i class="fa fa-circle text-danger"></i>Live</a></li>

              </ul>
            <ul class="menu-sidebar">
              <li><a href="#"><i class="fa fa-hourglass-end"></i>Lịch sử</a></li>
              <li><a href="#"><i class="fa fa-clock-o"></i>Xem sau</a></li>
            </ul>

            <ul class="menu-sidebar">
              <li><a href="{{ route('category',['categoryName'=>'music']) }}"><i class="fa fa-music text-danger"></i>Video nhạc hay</a></li>
              <li><a href="{{ route('category',['categoryName'=>'movies']) }}"><i class="fa fa-film text-danger"></i>Phim hay</a></li>
              <li><a href="{{ route('category',['categoryName'=>'game']) }}"><i class="fa fa-gamepad text-danger"></i>Games</a></li>
              {{-- <li><a href="{{ route('category',['categoryName'=>'sport']) }}"><i class="fa fa-soccer-ball-o text-danger"></i>Thể thao</a></li> --}}
            </ul>

            <ul class="menu-sidebar">
                <li><a href="#"><i class="fa fa-gear"></i>Settings</a></li>
                <li><a href="#"><i class="fa fa-question-circle"></i>Help</a></li>
                <li><a href="#"><i class="fa fa-send-o"></i>Send feedback</a></li>
              </ul>
              </div><!-- // sidebar-stick -->
              <div class="clear"></div>
          </div><!-- // left-sidebar -->
      </aside>
      <!-- // header -->
      @yield('content')

       <footer class="row clear" style="background: #a6aeaf; padding: 20px; color:#000">
        <div class="container-full">
            <div class="row" >
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="pull-left">
                        <a href="{{ url('/') }}"><img src="{{ asset('templates/video-post/img/logo2.png') }}" alt=""></a>
                    </div>
                    <div class="pull-left sel" style="margin-top: 9px;">
                        <div class="item-sel">
                            <form action="{{ route('switchLang') }}" class="form-lang" method="post">
                                <select name="locale" onchange='this.form.submit();'>
                                    <option value="en">{{ trans('app.lang.en') }}</option>
                                    <option value="vi" {{ App::getLocale() === 'vi' ? 'selected' : '' }}>{{ trans('app.lang.vi') }}</option>
                                    <option value="jp" {{ App::getLocale() === 'jp' ? 'selected' : '' }}>{{ trans('app.lang.jp') }}</option>
                                    <option value="fr" {{ App::getLocale() === 'fr' ? 'selected' : '' }}>{{ trans('app.lang.fr') }}</option>
                                </select>
                                {{ csrf_field() }}
                            </form>
                        </div>
                        <div class="item-sel">
                            <p data-toggle="modal" data-target="#myLocation"><i class="fa fa-map-marker"></i> Vị trí: {{ Session::get('location')->country }}
                              {{-- <i class="fa fa-caret-down"></i> --}}</p>
                        </div>
                        <div class="item-sel">
                            <p><i class="fa fa-hourglass-end"></i> Lịch sử</p>
                        </div>
                    </div>
                    <div class="pull-right sel" style="margin-top: 9px;">
                        Copyright © 2007-2018 Kênh video tổng hợp miễn phí
                    </div>
                </div>
            </div>
        </div>
    </footer>

      <script src="{{ asset('templates/video-post/js/jquery.sticky-kit.min.js') }}"></script>
      <script src="{{ asset('templates/video-post/js/custom.js') }}"></script>
      <script src="{{ asset('templates/video-post/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('templates/video-post/js/imagesloaded.pkgd.min.js') }}"></script>
      <script src="{{ asset('templates/video-post/js/grid-blog.min.js') }}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/vanilla-lazyload/8.6.0/lazyload.min.js"></script>
      <script type="text/javascript">
      $(document).ready(function(){
          $('.search-form input[type="text"]').on("keyup input", function(){
              var resultBox = $(".result");
              resultBox.empty();
              var key = $(this).val();
              if(key.length){
                var result='';
                var data = {
                      hl: "vi",
                      q: key,
                      client: "youtube",
                  };
                var googleAPI = "http://google.com/complete/search?callback=?";
                $.getJSON( googleAPI, data)
                .done(function(data) {
                  $.each( data[1], function( i, item ) {
                    $( "<p>" ).text(item[0]).appendTo(resultBox);
                  });
                  resultBox.show();
                });
              }else{
                  resultBox.empty();
                  resultBox.hide();
              }

          });

          // Set search input value on click of result item
          $(document).on("click", ".result p", function(){
              $(this).parents(".search-form").find('input[type="text"]').val($(this).text());
              $(this).parent(".result").empty().hide();
              $('#search').submit();
          });

          $(document).on("mouseleave", ".result", function(){
              $(this).hide();
          });
      });
      </script>
      <script type="text/javascript">
        (function() {
          $('.toggle-wrap').on('click', function() {
            $(this).toggleClass('active');
            $('aside').animate({width: 'toggle'}, 200);
          });
        })();
      </script>
      <script type="text/javascript">
        var myLazyLoad = new LazyLoad();
      </script>
   </body>
</html>
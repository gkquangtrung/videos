<?php

return [

    'lang' => [
      'vi' => 'Tiếng Việt',
      'en' => 'English',
      'jp' => 'Japanese',
      'fr' => 'Français',
    ],
    'home' => 'Page d\'accueil',
    'trending' => 'Tendance',
    'views' => 'vues',
    'year' => 'an',
    'month' => 'mois',
    'week' => 'la semaine',
    'day' => 'journée',
    'hour' => 'heure',
    'minute' => 'minute',
    'second' => 'seconde',
    'before' => 'avant',
    'just_moment' => 'juste le moment',
    'featured_videos' => 'Vidéos en vedette',
    'music' => 'musique',
    'film' => 'film',
    'game' => 'jeux',
    'sport' => 'sport',
    '' => '',
    '' => '',
    '' => '',

];
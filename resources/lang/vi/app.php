<?php

return [

    'lang' => [
      'vi' => 'Tiếng Việt',
      'en' => 'English',
      'jp' => 'Japanese',
      'fr' => 'Français',
    ],
    'home' => 'Trang chủ',
    'trending' => 'Xu hướng',
    'views' => 'lượt xem',
    'year' => 'năm',
    'month' => 'tháng',
    'week' => 'tuần',
    'day' => 'ngày',
    'hour' => 'giờ',
    'minute' => 'phút',
    'second' => 'giây',
    'before' => 'trước',
    'just_moment' => 'vừa xong',
    'featured_videos' => 'Video nổi bật',
    'music' => 'nhạc',
    'film' => 'phim',
    'game' => 'game hay',
    'sport' => 'thể thao',

];
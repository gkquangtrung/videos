<?php

return [

    'lang' => [
      'vi' => 'Tiếng Việt',
      'en' => 'English',
      'jp' => 'Japanese',
      'fr' => 'Français',
    ],
    'home' => 'Home',
    'trending' => 'Trending',
    'views' => 'views',
    'year' => 'year',
    'month' => 'month',
    'week' => 'week',
    'day' => 'day',
    'hour' => 'hour',
    'minute' => 'minute',
    'second' => 'second',
    'before' => 'before',
    'just_moment' => 'just moment',
    'featured_videos' => 'Featured videos',
    'music' => 'music',
    'film' => 'film',
    'game' => 'game',
    'sport' => 'sport',
    '' => '',

];
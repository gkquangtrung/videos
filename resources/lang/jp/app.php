<?php

return [

    'lang' => [
      'vi' => 'Tiếng Việt',
      'en' => 'English',
      'jp' => 'Japanese',
      'fr' => 'Français',
    ],
    'home' => '自宅',
    'trending' => 'トレンド',
    'views' => '再生回数',
    'year' => '年',
    'month' => '月',
    'week' => '週',
    'day' => '日',
    'hour' => '時間',
    'minute' => '分',
    'second' => '秒',
    'before' => '前に',
    'just_moment' => 'ただの瞬間',
    'featured_videos' => '注目の動画',
    'music' => '音楽',
    'film' => '膜',
    'game' => 'ゲーム',
    'sport' => 'スポーツ',
    '' => '',
    '' => '',
    '' => '',

];